-- =============================================
-- Author:		Brad Knowles
-- Create date: 26 JAN 2016
-- Description:	Removes non-printable control characters
-- =============================================
CREATE FUNCTION [dbo].[RemoveControlCharacters] 
(
	@RawText NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN	
	DECLARE @WorkingText NVARCHAR(MAX) = @RawText

	DECLARE @Characters TABLE( SomeChar NCHAR(1) NOT NULL )

	INSERT	@Characters( SomeChar )
		SELECT	NCHAR(0x0084) UNION ALL	-- http://www.fileformat.info/info/unicode/char/0084/index.htm
		SELECT	NCHAR(0x0080) UNION ALL	-- http://www.fileformat.info/info/unicode/char/0080/index.htm
		SELECT	NCHAR(0x009D)			-- http://www.fileformat.info/info/unicode/char/009D/index.htm

	SELECT	@WorkingText = REPLACE(@WorkingText, SomeChar, '') FROM @Characters
	RETURN (@WorkingText)
END
GO


