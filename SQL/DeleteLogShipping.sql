EXEC master.dbo.sp_delete_log_shipping_primary_secondary
@primary_database = N'<<LocalDBName>>'
,@secondary_server = N'<<SecondaryServerIPorName>>'
,@secondary_database = N'<<RemoteDBName>>'
GO

USE master;
GO
EXEC sp_delete_log_shipping_primary_database N'<<LocalDBName>>'