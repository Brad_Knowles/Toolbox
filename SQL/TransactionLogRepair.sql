-- Run this to determine why the transaction log isn't truncating
SELECT log_reuse_wait_desc,* FROM sys.databases

-- Some explanations of the results of the above query
-- http://sqlity.net/en/1805/eight-reasons-transaction-log-files-keep-growing/

-- If log_reuse_wait_desc says LOG_BACKUP, perform a Transaction Log Backup.
-- This may need to be done multiple times in order to clear up VLFs
-- https://www.sqlskills.com/blogs/paul/why-is-log_reuse_wait_desc-saying-log_backup-after-doing-a-log-backup/
-- https://blogs.msdn.microsoft.com/sql_pfe_blog/2013/06/27/lazy-log-truncation-clearing-of-sql-transaction-log-vlf-status-deferred/

-- Actually shrinking the log
USE <<DatabaseName>>
DBCC SHRINKLOG (200)

-- http://sqlmaestros.com/sql-server-dbcc-loginfo/
-- https://dotnetstories.wordpress.com/2012/12/28/looking-into-the-dbcc-loginfo-and-dbcc-sqlperf-commands/

DBCC LOGINFO(<<DatabaseName>>)

-- See if any open transaction are running
DBCC OPENTRAN(<<DatabaseName>>)
