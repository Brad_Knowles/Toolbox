﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            char IND = '\x0084';
            char PAD = '\x0080';
            char OSC = '\x009D';

            char[] charactersToRemove = {'[', IND, PAD, OSC, ']' };
            String pattern = new String(charactersToRemove);

            string input = "43507	For Use With Toolboxes Up To 70â Wide                                         ";
            String sanitizedInput = Regex.Replace(input, pattern, String.Empty);
        }
    }
}
